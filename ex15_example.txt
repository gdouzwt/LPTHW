Everything you've learned about raw_input and argv is so you can start reading files. You may
have to play with this exercise the most to understand what's going on, so do it carefully and
remember your checks. Working with files is an easy way to erase your work if you are not careful.

This exercise involves writing two files. One is your usual ex15.py file that you will run, but the
other is named ex15_example.txt. This second file isn't a script but a plain text file we'll be read-
ing in our script. Here are the contents of that file:

	This is stuff I typed into a file.
	It is really cool stuff.
	Lots and lots of fun to have in here.

What we want to do is "open" that file in our script and print it out. However, we do not want
to just "hard code" the name ex15_example.txt into our script. "hard coding" means putting 
some bit of information that should come from the user as a string right in our program. That's
bad because we want it to load other files later. The solution is to use argv and raw_input to ask
the user what file the user wants instead of "hard coding" the file's name.